angular.module('btDivPanel', [])
  .directive('divPanel', ['$compile',
    function($compile) {
      return {
        strict: 'E',
        templateUrl: '/partials/div_panel.html',
        transclude: true,
        scope: {
          title:    "@",
          id:       "@panelId",
          opened:   "@",
          classes:  "@panelClass"
        },
        link: function(scope, elem, attrs) {
          if(scope.opened === "true") {
            elem.find('.panel-collapse').addClass('in');
            scope.st = true;
            var sign = angular.element('<checkbox-sign />');
            $compile(sign)(scope);
            elem.find('.panel_header_link').append(sign);
          } else
            scope.st = false;

          if(scope.classes !== 'undefined' && scope.classes) {
            var classesArr = scope.classes.split(' ');
            angular.forEach(classesArr, function(value, key) {
              elem.children().addClass(value);
            });
          }
        }
      }
    }
  ])
  .directive('divPanelRadio', ['$compile',
    function($compile) {
      return {
        strict: 'E',
        templateUrl: '/partials/div_panel_radio.html',
        link: function(scope, elem, attr) {
          elem.bind("click", function(e) {
            scope.st = !scope.st;
            if(scope.st === true) {
              var sign = angular.element('<checkbox-sign />');
              $compile(sign)(scope);
              elem.parent().append(sign);
            } else
              elem.parent().find('.sign').remove();
          });
        }
      }
    }
  ])
  .directive('checkboxSign', [
    function () {
      return {
        strict: 'E',
        replace: true,
        template: '<div class="sign"></div>'
      }
    }
  ])
;