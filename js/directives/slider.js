var sliderApp = angular.module('sliderApp', []);

sliderApp.directive('slider', function($timeout) {
  return {
    restrict: 'AE',
    templateUrl: '../../partials/slider.html',
    scope: {
      images: '='
    },
    link: function(scope, elem, attrs) {
      scope.thumbWidth  = 97;
      scope.thumbHeight = 97;
      scope.galleryPath = '/images/gallery/';
      scope.galleryThumbPath = scope.galleryPath + 'thumbs/';
    }
  };
});
