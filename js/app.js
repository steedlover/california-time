var app = angular.module('app', ['ui.select'
  , 'ngSanitize'
  , 'btDivPanel'
  , 'sliderApp'
  , 'angular-flexslider'
  , 'videosharing-embed'
  , 'smart-table',
  , 'ngAnimate'
])
  .controller('PageCtrl', function($scope) {

    $scope.catDef = "";
    $scope.catOptions = [
      {
        name: 'Option 1'
      },
      {
        name: 'Option 2'
      },
      {
        name: 'Option 3'
      }
    ];

    $scope.amountClear = function() {
      $scope.amount = {};
      $scope.amount.start = "";
      $scope.amount.discount = false;
      $scope.full_amount = 0;
    };

    $scope.amountClear();
    $scope.calcAmount = function() {
      $scope.full_amount = $scope.amount.start;
      if($scope.amount.discount === true
        && $scope.amount.discount_amount < $scope.amount.start)
        $scope.full_amount -= $scope.amount.discount_amount
    };

    $scope.appointments = [
      {
        name: "Meeting with cafe",
        interviewer: "George"
      },
      {
        name: "Internet call via skype",
        interviewer: "Dmitry"
      }
    ];

    $scope.servForCustomersOptionDef = "";
    $scope.servForCustomerCurOption = {};
    $scope.servForCustomerFullList = [
      {
        name: "Travel to Customer (10 miles)",
        cost: "10",
        duration: "90 Minutes"
      },
      {
        name: "Customer comes to me",
        cost: "",
        duration: "1 Hour"
      }
    ];

    $scope.setHoursForCurrenOption = function(val) {
      $scope.servForCustomerCurOption.hours = val;
    };

    $scope.setMinutesForCurrenOption = function(val) {
      $scope.servForCustomerCurOption.minutes = val;
    };

    $scope.setNameForCurrenOption = function(val) {
      $scope.servForCustomerCurOption.name = val;
    };

    $scope.removeServFromList = function(index) {
      $scope.servForCustomerFullList.splice(index, 1);
    };

    $scope.addServForCustom = function() {
      $scope.servForCustomerCurOption.duration = "";
      var hours = $scope.servForCustomerCurOption.hours !== ""
        && $scope.servForCustomerCurOption.hours
        ? $scope.servForCustomerCurOption.hours
        : "";
      var minutes = $scope.servForCustomerCurOption.minutes !== ""
        && $scope.servForCustomerCurOption.minutes
        ? $scope.servForCustomerCurOption.minutes
        : "";

      if(hours !== "")
        $scope.servForCustomerCurOption.duration += hours;
      if($scope.servForCustomerCurOption.duration !== "" && minutes !== "")
        $scope.servForCustomerCurOption.duration += ' & ';
      $scope.servForCustomerCurOption.duration += minutes;
      $scope.servForCustomerFullList.push($scope.servForCustomerCurOption);
      $scope.servForCustomerCurOption = {};
    };

    $scope.titlesOptions = [
      "Travel to Customer (10 miles)",
      "Customer comes to me",
      "Drink a cup of coffee with client",
      "Play the video game"
    ];

    $scope.hoursOptions = [
      "1 Hour",
      "2 Hours",
      "3 Hours",
      "5 Hours",
      "10 Hours"
    ];

    $scope.minutesOptions = [
      "10 Minutes",
      "25 Minutes",
      "30 Minutes",
      "45 Minutes"
    ];

    /* Slider block */
    $scope.galleryFiles = [
      {
        image: 'hmmwv_1.jpg',
        description: 'Military HMMWV. Image#1',
        type: 'image'
      },
      {
        image: 'hmmwv_5.jpg',
        video: 'https://www.youtube.com/watch?v=-hjZXRi_ZtU',
        description: 'Video',
        type: 'video'
      },
      {
        image: 'hmmwv_2.jpg',
        description: 'Military HMMWV. Image#2'
      },
      {
        image: 'hmmwv_3.jpg',
        description: 'Military HMMWV. Image#3'
      },
      {
        image: 'hmmwv_4.jpg',
        description: 'Military HMMWV. Image#4'
      },
      {
        image: 'h2_1.jpg',
        description: 'Civil "Hummer". Image#1'
      },
      {
        image: 'h2_2.jpg',
        description: 'Civil "Hummer". Image#2'
      },
      {
        image: 'h2_3.jpg',
        description: 'Civil "Hummer". Image#3'
      }
    ];

  })

  .filter('moneyFormat', function() {
  return function(str) {
    str = Math.ceil(str);
    return str.toMoney(0, '.', ',');
  }
})
;
